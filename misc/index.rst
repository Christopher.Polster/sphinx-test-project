Welcome to garden's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. automodule:: garden

.. toctree::
    :maxdepth: 1

    places
    plants
    actions
    my_first_garden


Sphinx Links
============

- https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
- https://www.jetbrains.com/pycharm/guide/tutorials/sphinx_sites/
- https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
- https://github.com/tox-dev/sphinx-autodoc-typehints


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
