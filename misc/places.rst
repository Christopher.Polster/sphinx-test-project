Places in your garden where things can grow
===========================================

.. automodule:: garden.places
    :members:
