"""Growable plants for the garden"""


class Plant:
    """Base class for all growable plants."""



class Tree(Plant):
    """A tree that can carry fruit.

    Attributes
    ----------
    fruit: string
        The fruit carried by the tree.
    """

    def __init__(self, fruit=None):
        self.fruit = fruit

    def harvest(self, strength):
        """Shake the tree to harvest the fruit.

        Parameters
        ----------
        strength: `Number`
            The strength with which the tree is shaken. If shaken too little,
            the fruit might not fall. If shaken too hard, the tree might break.

        Returns
        -------
        A collection of fruit, possibly empty.
        """



class Cactus(Plant):
    """A prickly cactus."""

