"""Things to do in your garden."""

from typing import Union
from numbers import Number

from .places import Place
from .plants import Plant


def plant(what: Plant, where: Place):
    """Plant something somewhere.

    It's generally a good idea to :py:func:`fertilize` after planting so the
    plant has optimal growing conditions.

    Parameters
    ----------
    what
    where

    Example
    -------
    >>> patch = garden.Patch()
    >>> tree = garden.Tree()
    >>> plant(tree, garden)
    """


def fertilize(what: Union[Plant, Place], amount: Number):
    """Add fertilizer to a plant or place.

    Parameters
    ----------
    what:
        If a place is fertilized, all plants in that place recieve the same
        amount of fertilizer. For more control over the amount of fertilizer
        distributed, fertilize each plant individually.
    amount:
        How much fertilizer to apply in grams.

    Raises
    ------
    ValueError
        You have run out of fertilizer.
    """


def water(what: Union[Plant, Place]):
    ...

