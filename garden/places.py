"""Places in the garden where plants can grow."""

from numbers import Number
from math import pi as PI


class Place:
    """Base class for all places where plants can grow.

    Attributes
    ----------
    water_level: number
        Use :py:func:`.actions.water` to increase the water level.
    nutrient_level: number
        Use :py:func:`.actions.fertilize` to increase the water level.
    plants: Plant[]
        The plants planted in this place.
    """

    def __init__(self):
        self.water_level = 0.
        self.nutrient_level = 0.
        self.plants = []


class Patch(Place):
    """A patch of soil in the garden

    Attributes
    ----------
    length:
        The length of the patch in cm.
    width:
        The width of the patch in cm.
    """

    def __init__(self, length: Number, width: Number):
        super().__init__()
        self.length = length
        self.width = width

    @property
    def area(self) -> Number:
        """The plantable area of the pot in cm²."""
        return self.length * self.width


class Pot(Place):
    """A round planter that can be placed in- and outdoors.

    Attributes
    ----------
    radius:
        The upper radius of the pot in cm.
    """

    def __init__(self, radius: Number):
        super().__init__()
        self.radius = radius

    @property
    def area(self) -> Number:
        """The plantable area of the pot in cm²."""
        return 2 * PI * self.radius

